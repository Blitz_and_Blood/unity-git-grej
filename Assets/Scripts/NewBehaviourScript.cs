using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    float horizontal;
    float vertical;
    float speed = 8f;
    [SerializeField] Rigidbody2D rb;
    private void Update()
    {
        horizontal = Input.GetAxisRaw("Horizontal");
        vertical = Input.GetAxisRaw("Vertical");
        rb.velocity = new Vector2 (horizontal * speed, vertical * speed);
    }
}

