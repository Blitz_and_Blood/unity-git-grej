using UnityEngine;

public class Character2DController : MonoBehaviour
{
	public float MovementSpeed = 1;
	public float JumpForce = 1;

	public NewBehaviourScriptShoot ProjectilePrefab;
	public Transform LaunchOffset;

	private Rigidbody2D _rigidbody;

	private void Start()
	{
		_rigidbody = GetComponent<Rigidbody2D>();
	}

	private void Update()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			Instantiate(ProjectilePrefab, LaunchOffset.position, transform.rotation);
		}
	}
}
